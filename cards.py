import random
class Card:
    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank
    def __str__(self):
        return f'{list(self.rank.keys())[0]} of {self.suit}'

class Deck:
    def __init__(self):
        self.deck = []
        suits = ['hearts', 'diamonds', 'spades', 'clubs']
        ranks = {
            'A': 11,
            '2': 2,
            '3': 3,
            '4': 4,
            '5': 5,
            '6': 6,
            '7': 7,
            '8': 8,
            '9': 9,
            '10': 10,
            'J': 10,
            'Q': 10,
            'K': 10
        }

        for suit in suits:
            for rank in ranks:
                self.deck.append(Card(suit, {rank: ranks[rank]}))
                # self.deck.append([suit, {rank: ranks[rank]}])
    def shuffle(self):
        random.shuffle(self.deck)
        # random.shuffle(self.deck)

    def deal(self,n=1):
        cards_dealt = []
        for i in range(n):
            if len(self.deck) > 0:
                cards_dealt.append(self.deck.pop())
        return cards_dealt
