
import cards
class Hand:
    def __init__(self, dealer=False):
        self.cards = []
        self.value = 0
        self.dealer = dealer
    def add_card(self, card_list):
        self.cards.extend(card_list)
    def calc_value(self):
        self.value = 0
        is_ace = False
        for card in self.cards:
            self.value += list(card.rank.values())[0]
            if list(card.rank.keys())[0] == 'A':
                is_ace = True

        if self.value > 21 and is_ace:
            self.value -= 10
        return self.value

    def is_blackjack(self):
        return self.calc_value() == 21

    def show_hand(self, show_all =False):
        print(f'''{"Dealer's" if self.dealer else "Your"} hand: ''')
        for index, card in enumerate(self.cards):
            if index ==0 and self.dealer and not show_all and not self.is_blackjack():
                print("Dealer card - hidden")
            else:
                print(card)
        if not self.dealer:
            print(f"Value: {self.calc_value()}")


class Game:
    def play(self):
        game_number = 0
        games_to_play = 0

        while games_to_play <=0:
            try:
                games_to_play = int(input("How many games do you want to play? "))
            except:
                print("Please enter a valid number")
        while game_number < games_to_play:
            game_number += 1
            print("*" * 30)
            print(f"Game {game_number} of {games_to_play}:")
            input("Print any key to start a game")

            deck = cards.Deck()
            deck.shuffle()

            player_hand = Hand()
            dealer_hand = Hand(dealer =True)

            for i in range(2):
                player_hand.add_card(deck.deal())
                dealer_hand.add_card(deck.deal(1))

            player_hand.show_hand()
            dealer_hand.show_hand()

            if self.check_winner(player_hand, dealer_hand):
                continue

            choice = ""
            while player_hand.calc_value() < 21 and choice not in ['stand', 's']:
                choice = input("Please choose 'Hit(h)' or 'Stand(S)': ").lower()
                print("")
                while choice not in ['h', 's', 'hit','stand']:
                    choice = input("Please choose 'Hit/h' or 'Stand/s': ").lower()
                if choice in ['hit', 'h']:
                    player_hand.add_card(deck.deal(1))
                    player_hand.show_hand()

            if self.check_winner(player_hand, dealer_hand):
                continue

            player_hand_value = player_hand.calc_value()
            dealer_hand_value = dealer_hand.calc_value()

            while dealer_hand_value < 17:
                dealer_hand.add_card(deck.deal(1))
                dealer_hand_value = dealer_hand.calc_value()

            dealer_hand.show_hand(show_all=True)
            if self.check_winner(player_hand, dealer_hand):
                continue

            print(f"""Final Results: 
            Your Hand: {player_hand_value} 
            Dealer Hand: {dealer_hand_value}
            """)

            self.check_winner(player_hand, dealer_hand, game_over=True)

        print("Thanks for playing!")

    def check_winner(self, player_hand, dealer_hand, game_over=False):
        if not game_over:
            if player_hand.calc_value() > 21:
                print("You busted, dealer wins")
                return True
            elif dealer_hand.calc_value() > 21:
                print("Dealer busted, you win")
                return True
            elif player_hand.is_blackjack() and dealer_hand.is_blackjack():
                print("It's a Tie!")
                return True
            elif player_hand.is_blackjack():
                print("Blackjack! You Win!")
                return True
            elif dealer_hand.is_blackjack():
                print("Dealer Blackjack. Dealer wins!")
                return True
        else:
            if player_hand.calc_value() > dealer_hand.calc_value():
                print("You Win!")
            elif player_hand.calc_value() == dealer_hand.calc_value():
                print("It's a Tie!")
            else:
                print("Dealer Wins!")
            return True
        return False


g = Game()
g.play()


